package main

import "time"

var systemLocation, _ = time.LoadLocation("Brazil/East")
var swAPIConn SwAPIConnector

type PlanetRepository interface {
	Init(error)
	Create(p Planet) (Planet, error)
	ReadByName(name string) (Planet, error)
	ReadById(id string) (Planet, error)
	ReadAll() ([]Planet, error)
	DirectRead(field string, value string) (Planet, error)
	Update(p Planet) error
	Delete(p Planet) error
}
