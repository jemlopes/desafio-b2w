package main

import (
	"crypto/tls"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
)

type SwAPIConnector struct{}

func (swc SwAPIConnector) GetSwAPIPlanet(p *Planet) (SwAPIPlanet, error) {
	if p.SwapiID > 0 {
		return swc.GetSwAPIPlanetById(p.SwapiID)
	} else {
		return swc.GetSwAPIPlanetByName(p.Name)
	}
}

func (swc SwAPIConnector) GetSwAPIPlanetByName(swPlanetName string) (SwAPIPlanet, error) {
	server := config.App.SwApiUrl + "?search=" + swPlanetName
	return swc.getSwAPIPlanet(server)
}

func (swc SwAPIConnector) GetSwAPIPlanetById(swPlanetID int) (SwAPIPlanet, error) {
	server := config.App.SwApiUrl + strconv.Itoa(swPlanetID)
	return swc.getSwAPIPlanet(server)
}

func (swc SwAPIConnector) getSwAPIPlanet(url string) (SwAPIPlanet, error) {
	server := url
	var swp SwAPIPlanet
	transCfg := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // disable verify
	}
	client := &http.Client{Transport: transCfg}

	response, err := client.Get(server)
	if err != nil {
		logc.Errorf("Erro enviando request para swapi - %s", err)
	}
	defer response.Body.Close()

	data, err := ioutil.ReadAll(response.Body)

	if err != nil {
		logc.Errorf("Erro parsing body para swapi - %s", err)
	} else {
		swpSearch := SwAPISearch{}
		err = json.Unmarshal(data, &swpSearch)
		if err == nil && swpSearch.Count > 0 {
			swp = swpSearch.Results[0]
		}
	}

	return swp, err
}
