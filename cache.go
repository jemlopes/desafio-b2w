package main

import (
	"container/heap"
	"encoding/json"
	"strings"
	"time"

	"github.com/streamrail/concurrent-map"
	"github.com/valyala/fasthttp"
)

type Cacheable interface {
	cacheID() string
	timeStamp() time.Time
}

type Kache struct {
	cacheBag       cmap.ConcurrentMap
	cacheMeta      cmap.ConcurrentMap
	servers        map[string]bool
	connectionPool []bool
}

type Connection struct {
	isAvailable bool
}

var bheap = &broadcastHeap{}

var cacheLocation, _ = time.LoadLocation("Brazil/East")

func (c *Kache) StartCache() {
	c.cacheBag = cmap.New()
	c.cacheMeta = cmap.New()
	c.servers = make(map[string]bool)
	c.connectionPool = make([]bool, config.Cache.DistributedPooling)
	heap.Init(bheap)
	var configServers []string
	if strings.Index(config.Cache.DistributionPoints, ",") > 0 {
		configServers = strings.Split(config.Cache.DistributionPoints, ",")
	} else {
		configServers = []string{config.Cache.DistributionPoints}
	}

	for i := 0; i < len(configServers); i++ {
		c.servers[configServers[i]] = true
	}

	for i := 0; i < config.Cache.DistributedPooling; i++ {
		c.connectionPool[i] = true
	}

	go c.cacheManager()

	go c.broadcast()

}

func (c *Kache) Save(p Planet) error {
	var err error
	c.save(p)
	if config.Cache.UseDistributed {
		go c.sendToBroadcast(p.ID)
	}
	return err
}

func (c *Kache) save(p Planet) error {
	logc.Debugf("Saving in Cache: %s", p.ID)
	c.cacheBag.Set(p.ID, p)
	c.cacheMeta.Set(p.ID, time.Now().In(cacheLocation))
	return nil
}

func (c *Kache) Remove(key string) {
	c.cacheBag.Remove(key)
}

func (c *Kache) Get(key string) (Planet, bool) {
	p := Planet{}
	ok := false
	cacheItem, ok := c.cacheBag.Get(key)
	
	if ok {
		p = cacheItem.(Planet)
	}

	return p, ok
}

func (c *Kache) GetByName(key string) (Planet, bool) {
	p := Planet{}
	ok := false
	cacheItem, ok := c.cacheBag.Get(key)
	
	if ok {
		p = cacheItem.(Planet)
	}

	return p, ok
}


func (c *Kache) GetJson(key string) ([]byte, bool) {
	pj := []byte{}

	p, ok := c.cacheBag.Get(key)
	if ok {
		pj, _ = json.Marshal(&p)
	} else {
		logc.Errorf("Not found in Cache(Json):  %s", key)
	}

	return pj, ok
}

func (c *Kache) cacheManager() {
	var duration time.Duration
	for {
		time.Sleep(time.Duration(config.Cache.UpdateIntervalinMillis) * time.Millisecond)
		logc.Debugf("Cache loop:")
		logc.Debugf("Items in Cache: %d", c.cacheBag.Count())
		for item := range c.cacheMeta.Iter() {
			duration = time.Since(item.Val.(time.Time))
			logc.Debugf("Check: %s Times: %f vs %f (should delete? %t )", item.Key, duration.Minutes(), float64(config.Cache.ExpirationTimeinMinutes), duration.Minutes() > float64(config.Cache.ExpirationTimeinMinutes))
			if duration.Minutes() > float64(config.Cache.ExpirationTimeinMinutes) {
				logc.Debugf("Removing %s\n", item.Key)
				c.Remove(item.Key)
			}
		}

		logc.Debugf("Updated Items in Cache: %d", c.cacheBag.Count())
	}
}

func (c *Kache) sendToBroadcast(key string) {
	heap.Push(bheap, key)
}

func (c *Kache) broadcast() {
	noPlanetsMax := 1000
	for {
		time.Sleep(time.Duration(config.Cache.BroadCastTimeInMillis) * time.Millisecond)
		noPlanets := bheap.Len()

		if noPlanets > 0 {
			if noPlanets > noPlanetsMax {
				noPlanets = noPlanetsMax
			}
			Planets := []Planet{}
			//	Planets := []byte{}
			PlanetsKeys := bheap.PopMultiple(noPlanets)
			for i := 0; i < len(PlanetsKeys); i++ {
				Planet, _ := c.Get(PlanetsKeys[i])
				Planets = append(Planets, Planet)
				logc.Debugf("Broadcasting -> Planet: %s", Planet.ID)
			}
			PlanetsJSON, _ := json.Marshal(Planets)
			content := []byte{}
			content = fasthttp.AppendGzipBytes(content, PlanetsJSON)
			for host, send := range c.servers {
				if send {
					j := 0
					for {
						if c.connectionPool[j] == true {
							go c.broadcastToServer(j, host, content)
							break
						}
						j++
						if j >= config.Cache.DistributedPooling {
							j = 0
						}
					}
				}
			}
		}

	}
}

func (c *Kache) ReceiveBundle(bPlanets []byte, action string) {
	Planets := []Planet{}
	err := json.Unmarshal(bPlanets, &Planets)
	//Parse Planets, reject stale cache info
	logc.Debugf("Receiving cache bundle. Items to process= %d , %v ", len(Planets), err)
	for i := 0; i < len(Planets); i++ {
		p := Planets[i]
		u := false
		pc, exists := c.Get(p.ID)
		if exists {
			tn := time.Since(p.UpdatedAt)
			to := time.Since(pc.UpdatedAt)
			u = tn < to
		} else {
			u = true
		}
		if u {
			logc.Debugf("Updating cache item (%s)...", p.ID)
			c.save(p)
		} else {
			logc.Debugf("cache item (%s) is stale. Discarding...", p.ID)
		}
	}
}

func (c *Kache) broadcastToServer(conn int, host string, content []byte) error {
	c.connectionPool[conn] = false
	server := "http://" + host + ":" + config.App.Port + "/synccache/u"
	connector := &fasthttp.Client{
		Name:         host,
		WriteTimeout: 100 * time.Millisecond,
		ReadTimeout:  100 * time.Millisecond,
	}

	req := fasthttp.AcquireRequest()
	resp := fasthttp.AcquireResponse()
	req.Header.SetMethodBytes([]byte("POST"))
	req.Header.SetContentTypeBytes([]byte("application/kache"))
	req.SetRequestURI(server)
	req.AppendBody(content)

	resp.SkipBody = true
	logc.Debugf("start sending cache: %s", server)
	err := connector.DoTimeout(req, resp, 500*time.Millisecond)
	if err != nil {
		logc.Errorf("Error sending cache: %s. Disabling cache for server: %s", err, host)
	}
	if resp.Header.StatusCode() != fasthttp.StatusOK {
		logc.Errorf("Unexpected status code: %d. Expecting %d", resp.Header.StatusCode(), fasthttp.StatusOK)
	}
	logc.Debugf("end sending cache: %s", server)
	fasthttp.ReleaseRequest(req)
	fasthttp.ReleaseResponse(resp)
	c.connectionPool[conn] = true
	return err
}
