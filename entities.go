package main

import (
	"errors"
	"time"
)

type Planet struct {
	ID          string    `json:"id"`
	SwapiID     int       `json:"swapiId"`
	Name        string    `json:"name"`
	Climate     string    `json:"climate"`
	Terrain     string    `json:"terrain"`
	Apparitions int       `json:"apparitions"`
	CreatedAt   time.Time `json:"createdAt"`
	UpdatedAt   time.Time `json:"lastUpdate"`
}

type SwAPISearch struct {
    Count int `json:"count"`
	Results []SwAPIPlanet `json:"results"`
}

type SwAPIPlanet struct {
	ID        int       `json:"id"`
	Name      string    `json:"name"`
	Terrain   string    `json:"terrain"`
	Films     []string  `json:"films"`
	CreatedAt time.Time `json:"createdAt"`
	UpdatedAt time.Time `json:"lastUpdate"`
}

/*Validations */

func (pl *Planet) Validate() error {
	if len(pl.Name) < 1 {
		logc.Errorf("Nome e obrigatorio")
		return errors.New("Nome e obrigatorio")
	}

	if len(pl.Climate) < 1 {
		logc.Errorf("Clima e obrigatorio")
		return errors.New("Clima e obrigatorio")
	}
	if len(pl.Terrain) < 1 {
		logc.Errorf("Terreno e obrigatorio")
		return errors.New("Terreno e obrigatorio")
	}
	return nil
}

func copyPlanet(p1 *Planet, p2 *Planet) {
	*p1 = *p2
}


func (swp *SwAPIPlanet) GetApparitions() (int) {
return len(swp.Films)
}