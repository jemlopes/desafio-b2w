package main

import (
	"errors"
	"strings"
	"time"

	"github.com/gocql/gocql"
	"github.com/satori/go.uuid"
)

type CassandraRepository struct{}

var cluster *gocql.ClusterConfig

var CassandraSession *gocql.Session

func (r CassandraRepository) Init() (*gocql.Session, error) {
	var duration time.Duration

	var points []string
	if strings.Index(config.Cassandra.ContactPoints, ",") > 0 {
		points = strings.Split(config.Cassandra.ContactPoints, ",")
	} else {
		points = []string{config.Cassandra.ContactPoints}
	}
	cluster = gocql.NewCluster(points...)
	cluster.ProtoVersion = config.Cassandra.Protoversion
	cluster.NumConns = config.Cassandra.Connections
	cluster.Timeout = 30 * time.Second
	for {
		//Testa se Banco de dados está ativo

		sysSession, err := r.getSystemSession()
		if err == nil {
			//Verifica se banco está criado
			if err = sysSession.Query("CREATE KEYSPACE " + config.Cassandra.Keyspace + " WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 }; ").Exec(); err == nil {
				sysSession.Close()
				CassandraSession, err = r.getSession()

				err = CassandraSession.Query(`
							CREATE TABLE planets (
							id text,  
							internal_name text,  
							name text,
							swapi_id int,
							climate text,  
							terrain text,  
							created_at timestamp,  
							updated_at timestamp,
							PRIMARY KEY (id, name)
							);`).Exec()
				if err != nil {
					return nil, err
				}
			} else {
				CassandraSession, err = r.getSession()
			}

			if config.Cassandra.EnableFullConsistency {
				CassandraSession.SetConsistency(gocql.All)
			}
			break
		}
		cluster.Keyspace = config.Cassandra.Keyspace
		if duration.Minutes() > float64(config.Cassandra.InitTimeoutInSeconds) {
			return nil, errors.New("Cassandra server not available")
		}

		time.Sleep(time.Duration(1) * time.Second)
	}
	return CassandraSession, nil
}

func (r CassandraRepository) getSystemSession() (*gocql.Session, error) {
	cluster.Keyspace = "system"
	return cluster.CreateSession()
}

func (r CassandraRepository) getSession() (*gocql.Session, error) {
	cluster.Keyspace = config.Cassandra.Keyspace
	return cluster.CreateSession()
}

func (r CassandraRepository) ReadAll() ([]Planet, error) {
	var planets []Planet

	iter := CassandraSession.Query("SELECT id, name, climate, created_at, swapi_id, terrain, updated_at FROM planets").Iter()
	pt := Planet{}
	for iter.Scan(&pt.ID, &pt.Name, &pt.Climate, &pt.CreatedAt, &pt.SwapiID, &pt.Terrain, &pt.UpdatedAt) {
		p := Planet{}
		copyPlanet(&p, &pt)
		r.setupPlanet(&p)
		planets = append(planets, p)
	}
	if err := iter.Close(); err != nil {
		logc.Errorf("ReadAll:: %s", err)
	}
	return planets, nil
}

func (r CassandraRepository) Create(p Planet) (Planet, error) {
	u, err := uuid.NewV4()

	p.ID = u.String()

	p.CreatedAt = time.Now().In(systemLocation)
	p.UpdatedAt = p.CreatedAt

	if err = CassandraSession.Query("INSERT INTO Planets (id , swapi_id, name, internal_name, climate , terrain, created_at, updated_at) VALUES (?,?,?,?,?,?,?,?)", p.ID, p.SwapiID, p.Name, strings.ToLower(p.Name), p.Climate, p.Terrain, p.CreatedAt, p.UpdatedAt).Exec(); err != nil {
		logc.Errorf("Create:: %s ::: %s", p.ID, err)
		return p, err
	}

	if config.Cache.EnableCache {
		go Cache.Save(p)
	}

	return p, nil
}

func (r CassandraRepository) ReadById(id string) (Planet, error) {
	p := Planet{}
	cacheOk := false
	var err error
	if config.Cache.EnableCache {
		p, cacheOk = Cache.Get(id)
	}

	if !cacheOk {
		p, err = r.DirectRead("id", id)
		if config.Cache.EnableCache && err == nil {
			go Cache.Save(p)
		}
	}

	return p, err
}

func (r CassandraRepository) ReadByName(name string) (Planet, error) {
	p := Planet{}
	cacheOk := false
	var err error
	if config.Cache.EnableCache {
		p, cacheOk = Cache.GetByName(name)
	}

	if !cacheOk {
		p, err = r.DirectRead("name", strings.ToLower(name))
		if config.Cache.EnableCache && err == nil {
			go Cache.Save(p)
		}
	}
	return p, err
}

func (r CassandraRepository) DirectRead(field string, value string) (Planet, error) {
	p := Planet{}
	var query string
	if field == "id" {
		query = "SELECT id, name, climate, created_at, swapi_id,terrain, updated_at FROM planets WHERE id=?"
	} else {
		query = "SELECT id, name, climate, created_at, swapi_id, terrain, updated_at FROM planets WHERE internal_name=? ALLOW FILTERING"
	}

	if err := CassandraSession.Query(query, value).Scan(&p.ID, &p.Name, &p.Climate, &p.CreatedAt, &p.SwapiID, &p.Terrain, &p.UpdatedAt); err != nil {
		logc.Errorf("DirectRead:: field:%s :: value:%s ::: error:", field, value, err)
		return p, err
	}
	r.setupPlanet(&p)

	return p, nil
}

func (r CassandraRepository) Update(p Planet) error {
	p.UpdatedAt = time.Now().In(systemLocation)
	if err := CassandraSession.Query("UPDATE planets SET name=? , internal_name=?, climate=?, terrain=?, updated_at=? WHERE id=?", p.ID, p.Name, strings.ToLower(p.Name), p.Climate, p.Terrain, p.UpdatedAt, p.ID).Exec(); err != nil {
		logc.Errorf("Update:: %s ::: %s", p.ID, err)
		return err
	}

	if config.Cache.EnableCache {
		go Cache.Save(p)
	}
	return nil
}

func (r CassandraRepository) Delete(p Planet) error {
	if err := CassandraSession.Query("DELETE from planets where id = ?", p.ID).Exec(); err != nil {
		logc.Errorf("Delete:: %s ::: %s", p.ID, err)
		return err
	}

	if config.Cache.EnableCache {
		go Cache.Remove(p.ID)
	}
	return nil
}

func (r CassandraRepository) setupPlanet(p *Planet) {
	p.CreatedAt = p.CreatedAt.In(systemLocation)
	p.UpdatedAt = p.UpdatedAt.In(systemLocation)
	swp, swErr := swAPIConn.GetSwAPIPlanet(p)
	if swErr == nil {
		p.SwapiID = swp.ID
		p.Apparitions = swp.GetApparitions()
	}
}
