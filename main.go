package main

import (
	"fmt"
	"strings"
	"time"

	"github.com/kataras/iris"
	"github.com/valyala/fasthttp"
)

const (
	version = "   Jedi Archives Release: 1.0.0"
)

var config = getSettings()

var Cache Kache
var mainURI string
var repo CassandraRepository
var SystemLocation *time.Location

func main() {
	api := iris.New()
	var err error
	SystemLocation, _ := time.LoadLocation("Brazil/East")
	StartLogging(SystemLocation)
	if err != nil {
		api.Logger().Fatalf("nao foi possivel conectar ao cassandra: %s", err)
		return
	}

	repo = CassandraRepository{}
	session, err := repo.Init()
	if err != nil {
		api.Logger().Fatalf("nao foi possivel inicializar cassandra: %s", err)
	} else {
		defer session.Close()
	}

	fmt.Printf("Jedi Archives Configuration:\n%v\n\n", config)
	if config.Cache.EnableCache {
		Cache.StartCache()
	}

	logc.Infof("%s", version)
	logc.Infof("Using IRIS %s\n", iris.Version)

	mainURI = "/" + config.App.ApiVersion

	/*AWS Health Check*/
	api.Get("/", func(ctx iris.Context) {
		ctx.StatusCode(iris.StatusOK)
	})

	/* Cache integration API */
	api.Post("/synccache/{action:string}", func(ctx iris.Context) {
		action := ctx.Params().Get("action")
		if (action == "u" || action == "d") && config.Cache.UseDistributed {
			rec := ctx.Recorder()
			contentRaw := rec.Body()
			content := []byte{}
			content, err := fasthttp.AppendGunzipBytes(content, contentRaw)
			serverinfo := strings.Split(ctx.RemoteAddr(), ":")
			if config.Cache.EnableCache && config.Cache.UseDistributed {

				if !strings.Contains(config.Cache.DistributionPoints, serverinfo[0]) {
					logc.Errorf("RcacheSync Unauthorized Server: <%s>", serverinfo[0])
					ctx.StatusCode(iris.StatusUnauthorized)
				}

				if err != nil {
					logc.Errorf("RcacheSync Error: <%s>", err)
					ctx.StatusCode(iris.StatusBadRequest)
				}

				logc.Debugf("RCVCACHE: %s :: content: <%s> from server <%s>", action, content, serverinfo[0])
				go Cache.ReceiveBundle(content, action)
			} else {
				ctx.StatusCode(iris.StatusBadRequest)
			}
		} else {
			ctx.StatusCode(iris.StatusForbidden)
		}
	})

	/* Planets REST API */

	api.Post(mainURI+"/planets", func(ctx iris.Context) {
		planet := Planet{}
		err := ctx.ReadJSON(&planet)

		if err != nil {
			logc.Errorf("%s", err)
			ctx.StatusCode(iris.StatusBadRequest)
			return
		}

		planet, err = repo.Create(planet)

		if err != nil {
			ctx.StatusCode(iris.StatusInternalServerError)
		} else {
			ctx.Header("Location", fmt.Sprintf("%s/planets/%s", mainURI, planet.ID))
			ctx.StatusCode(iris.StatusCreated)
			ctx.Text("{\"id\": \"" + planet.ID + "\"}")
		}
	})

	api.Get(mainURI+"/planets", func(ctx iris.Context) {
		name := ctx.URLParamTrim("name")

		if name != "" {
			planet, err := repo.ReadByName(string(name))

			if err != nil {
				if err.Error() == "not found" {
					ctx.StatusCode(404)
				} else {
					ctx.StatusCode(iris.StatusInternalServerError)
				}
			} else {
				ctx.JSON(planet)
			}
		} else {
			planets, err := repo.ReadAll()
			if err != nil {
				ctx.StatusCode(iris.StatusInternalServerError)
			} else {
				ctx.JSON(planets)
			}
		}
	})

	/*Planet REST API */
	api.Get(mainURI+"/planets/{planetId:string}", func(ctx iris.Context) {
		planet, err := repo.ReadById(ctx.Params().Get("planetId"))

		if err != nil {
			if err.Error() == "not found" {
				ctx.StatusCode(404)
			} else {
				ctx.StatusCode(iris.StatusInternalServerError)
			}
		} else {
			ctx.JSON(planet)
		}
	})

	api.Put(mainURI+"/planets/{planetId:string}", func(ctx iris.Context) {
		planet, err := repo.ReadById(ctx.Params().Get("planetId"))
		if err == nil {
			repo.Update(planet)

			ctx.JSON(planet)
		} else {
			ctx.StatusCode(iris.StatusInternalServerError)
		}
	})

	api.Delete(mainURI+"/planets/{planetId:string}", func(ctx iris.Context) {
		planet, err := repo.ReadById(ctx.Params().Get("planetId"))
		if err == nil {
			repo.Delete(planet)
			ctx.StatusCode(200)
		} else {
			ctx.StatusCode(iris.StatusInternalServerError)
		}
	})

	api.Run(iris.Addr(":"+config.App.Port), iris.WithCharset("UTF-8"))

}
