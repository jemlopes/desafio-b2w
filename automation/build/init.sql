CREATE KEYSPACE jedi_archive  WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };
  
use jedi_archive;
  
CREATE TABLE planets (
  id text,  
  internal_name text,  
  name text,
  swapi_id int,
  climate text,  
  terrain text,  
  created_at timestamp,  
  updated_at timestamp,
  PRIMARY KEY (id, name)
  );

